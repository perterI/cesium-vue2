### cesium

#### Cesium 相机系统

- 相机飞行

  ```javascript
  viewer.camera.flyTo({
   destination:Cesium.Cartesian3.fromDegrees(x,y,height); // 飞行位置，将经纬度转为笛卡尔坐标
   duration:1,// 相机飞行时间，秒
   // 相机飞行后看的角度
   orientation:{
               heading: Cesium.Math.toRadians(0, 0),
               pitch: Cesium.Math.toRadians(-45), // 设置俯仰角度
               roll: 0.0
   }
  })
  ```

  相机切换视角

  ```javascript
  viewer.camera.setView({
  destination:Cesium.Cartesian3.fromDegrees(x,y,height); // 飞行位置，将经纬度转为笛卡尔坐标
  // 飞行的角度
  orientation:{
             heading: Cesium.Math.toRadians(0, 0),
             pitch: Cesium.Math.toRadians(-45), // 设置俯仰角度
             roll: 0.0
  }
  })
  ```

  ```javascript
  viewer.camera.lookUp(Cesium.Math.toRadians(10)); // 相机向上旋转多少度
  viewer.camera.lookDown(Cesium.Math.toRadians(10)); // 相机向下旋转多少度
  viewer.camera.lookLeft(Cesium.Math.toRadians(10)); // 相机向左旋转多少度
  viewer.camera.lookRight(Cesium.Math.toRadians(10)); // 相机向右旋转多少度

  viewer.camera.moveUp(100); // 相机向前平移
  viewer.camera.moveDown(100); // 相机整体向后平移
  viewer.camera.moveLeft(100); // 相机整体向左
  viewer.camera.moveRight(100); // 相机整体向右平移
  viewer.camera.moveForward(100); // 相机向正前方移动

  viewer.camera.twistLeft(Cesium.Math.toRadians(10)); // 整个地球的旋转（左）
  viewer.camera.twistRight(Cesium.Math.toRadians(10)); // 整个地球的旋转（右）
  viewer.camera.zoomIn(10); //相机向前移动，对焦
  viewer.camera.zoomOut(10); // 相机向后移动，对焦
  ```

#### Cesium 添加点，添加线|折线，添加带高度的线|折线，添加面，添加体，添加模型，添加 label 标签，添加广告牌

##### 全部可以添加的

```javascript
viewer.entities.add({
    name:'点',
    id:'textPoint', // 若用户没有设置id，会自动生成id作为唯一值
    position:Cesium.Cartesian3.fromDegrees(x,y,h)
    // 添加点
    point:{
        show:true,// 是否展示当前点
        pixelSize:10,// 指定当前点的大小
        heightReference:HeightReference.NONE,// 指定当前点相对于什么的高度
        color:Cesium.Color.ALICEBLUE,// 指定当前点的颜色信息
        outlineColor:Color.PINK,// 指定当前点的外边框
        outlineWidth:2, // 指定当前点的外边框的宽度
        translucencyByDistance:Cesium.DistanceDisplayCondition(10,20),// 指定相机在多远处显示
        disableDepthTestDistance:,// 指定当前点显示测试
    },
    // 添加广告牌
    billboard:{
        show:true|false,// 是否展示当前广告牌
        image:'所要显示的图片地址',
        scale:1.0,// 指定图像的大小比例
        pixelOffset:new Cesium.Cartesian2(8, 0),// 当前广告牌到点的偏移量，左右偏移量
        horizontalOrigin:HorizontalOrigin.CENTER,// HorizontalOrigin(水平的)
// CENTER LEFT RIGHT
        verticalOrigin:verticalOrigin.CENTER,// verticalOrigin(垂直的)
                // CENTER BOTTOM BASELINE TOP
        color:Color.WHITE,// 颜色
        rotation:,// 旋转的度数
        width:10,// 广告牌的宽
        height:10,// 广告牌的高
    },
    // 添加label标签
    label:{
        show:true,
        text:'', // 显示的文本内容
        font:'12px 微软雅黑',// 设置文本的字体样式
        style:LabelStyle.FILL,// 指定怎么显示Label样式
        scale:1.0,// 文本的比例
        showBackground:true|false,//是否展示背景颜色
        backgroundColor:new Color(0.165, 0.165, 0.165, 0.8),// 设置背景颜色
        backgroundPadding:new Cartesian2(7, 5),// 设置label的显示背景的大小
        pixelOffset:Cartesian2.ZERO,// 偏移量
        eyeOffset:Cartesian3.ZERO,// 眼睛的偏移量
        horizontalOrigin:HorizontalOrigin.CENTER,// 当前label距离点位的水平偏移量
        verticalOrigin:verticalOrigin.CENTER, // 当前label距离点位的垂直偏移量
        heightReference:HeightReference.NONE,// 指定当前高度对于什么
        fillColor:Color.WHITE,// 指定填充颜色
        outlineColor:Color.BLACK,// 指定外边框的颜色
        outlineWidth:1.0,// 指定当前label的外边框的宽
    },
    // 添加盒子
    box:{
        show:true|false,// 是否显示当前的盒子
        dimensions:new Cesium.Cartesian3(400000.0, 300000.0, 500000.0),// 默认为空，设置指定框的长度，宽度，高度。
        heightReference:,// 当前模型是相对于谁的高度
        fill:false|true,// 是否填充盒子
        material:Color.WHITE,// 指定当前填充盒子的材料或者是颜色
        outline:true|false,// 是否显示盒子的外边框.
        outlineColor:Color.BLACK,// 设置当前外边框的颜色
        outlineWidth:1.0,// 设置当前盒子外边框的宽度
        shadows:ShadowMode.DISABLED,// 设置盒子的阴影
    } ,
    // 添加走廊
    corridor:{
        show:true|false,// 设置是否显示
        positions:Cesium.Cartesian3.fromDegreesArray([
      -90.0,
      40.0,
      -95.0,
      40.0,
      -95.0,
      35.0,
    ]),// 设置指定走廊的位置信息
    width:10000,// 设置当前走廊的宽度
    height:20000,// 设置走廊的高度
    heightReference:HeightReference.NONE,// 设置走廊的高度是相对于什么的
    cornerType:CornerType.ROUNDED,// 设置走廊的弯道是什么样的，ROUNDED(圆角光滑的边缘) MITERED(直角边) BEVELED(被裁掉的角)
    fill:true|false,// 是否填充走廊
    material:Color.WHITE,// 走廊填充的材料或者是颜色
    outline:true|false, // 是否显示走廊的外边框
    outlineColor:Color.BLACK,// 走廊外边框的颜色
    outlineWidth:1.0,// 走廊外边框的宽度
    shadows:ShadowMode.DISABLED,// 设置走廊的阴影
    zIndex:9,// 设置当前走廊的层级关系
    },
    // 添加圆柱体，圆锥体
    cylinder:{
        show:true|false,// 是否显示圆柱
        length:20,// 设置圆柱体的高度
        topRadius:20,// 设置圆柱的顶部的半径
        bottomRadius:20,// 设置圆柱的底部的半径
        heightReference:HeightReference.NONE,// 设置圆柱的高度是对于谁的
        fill:true|false, // 是否填充颜色
        material:Color.WHITE, // 圆柱的填充色或者是填充的材料
        outline:true|false, // 是否显示圆柱的外边框
        outlineColor:Color.BLACK, // 设置外边框的颜色
        outlineWidth:1,// 设置外边框的宽度
        numberOfVerticalLines:16,// 设置圆柱显示外边框的数量
        slices:128, // 设置圆柱圆边的边数
        shadows:ShadowMode.DISABLED    , // 设置阴影
    },
    // 添加椭圆
    ellipse:{
        show:true|false,// 是否显示椭圆
        semiMajorAxis:3000, // 指定椭圆长轴的
        semiMinorAxis:4000,// 指定半短轴的长度
        height:0,// 指定椭圆距离地面的高度
        heightReference:HeightReference.NONE,// 指定椭圆的高度是相对于谁的
        extrudedHeight:0, // 指定椭圆的拉升高度（椭圆本生的高度）
        rotation:Cesium.Math.toRadians(45), // 旋转的角度.从北逆时针旋转
        stRotation:Cesium.Math.toRadians(45), // 从纹理北逆时针旋转
        granularity:128,// 设置椭圆点上之间的距离
        fill:true|false, // 设置是否填充
        material:Color.WHITE,// 填充的颜色或者是填充的材料
        outline:true|false, // 是否显示外边框
        outlineColor:Color.BLACK, // 外边框的颜色
        outlineWidth:1.0,// 外边框的宽度
        numberOfVerticalLines:128,// 设置椭圆垂直的线条
        shadows:ShadowMode.DISABLED, // 设置阴影
        zIndex:8, // 设置显示的层级
    },
    // 添加模型
 model:{
    show:true|false, // 是否显示模型
    uri:'需要的模型路径', // 模型路径
    scale:1,// 设置模型的比例
    minimumPixelSize:0,// 模型显示最小为多少
    maximumScale:200, // 模型显示最大为多少
    incrementallyLoadTextures:true|false,//
    shadows:ShadowMode.ENABLED,// 显示模型的阴影
    heightReference:HeightReference.NONE, // 当前显示模型的高度是相对于谁
    silhouetteColor:Color.RED,// 指定模型的轮廓
    silhouetteSize:1 , // 显示轮廓的大小，0为不显示
    color:Color.WHITE, // 显示模型的颜色，可以设置当前模型的颜色或者是透明度
 },
 orientation:Cesium.Transforms.headingPitchRollQuaternion('点位坐标',new Cesium.HeadingPitchRoll(heading角度, pitch, roll)),
 path :{
    show:true|false, // 是否展示路径
    leadTime:1,// 指定要显示的对象前面的秒数
    trailTime:,// 指定要显示的对象后面的秒数
    width:1,// 路径显示的宽度
    resolution:60,// 指定在对位置进行采样时进步的最大毫秒数
    material:Color.WHITE, // 设置路径的材质，或者是路径的颜色
    distanceDisplayCondition:120, // 指定当前路径在将在相机多远的距离来显示
 },

//  添加多边形
 polygon:{
    show:true|false, // 是否展示多边形
    hierarchy:Cesium.Cartesian3.fromDegreesArray([
      -115.0,
      37.0,
      -115.0,
      32.0,
      -107.0,
      33.0,
      -102.0,
      31.0,
      -102.0,
      35.0,
    ]), // 当前多边形显示的坐标
    height:0,// 距离地面的高度
    heightReference:HeightReference.NONE, // 当前多边形是对于谁的高度
    extrudedHeight:50000,// 当前多边形拉升的高度
    fill:true|false, // 是否显示填充色
    material:Color.RED,// 当前多边形显示的颜色或者是显示的材质
    outline:true|false, // 是否显示外边框
    outlineColor:Color.WHITE,// 外边框的颜色
    outlineWidth:1, // 显示外边框的宽度
    perPositionHeight:true|false, // 指定是否对于地形进行变化
    zIndex:9,// 显示的层级
    shadows:,// 显示阴影
 },
// 添加线
polyline:{
    show:true|false, // 是否展示当前的线
    positions:Cesium.Cartesian3.fromDegreesArray([123.91264, 41.86205, 123.91264, 40.86209]), // 指定当前线的坐标位置，Cesium.Cartesian3.fromDegressArray([经度，纬度，经度，纬度....])
// 添加有高度的线可以使用
    width:1.0, // 指定线的宽度
    material:Color.WHITE, // 指定当前线的材质或者是颜色
    clampToGround:true|false, // 是否固定在地表面
    shadows:ShadowMode.DISABLED, // 定义线的
    distanceDisplayCondition:'', // 定义当前折线在距离相机多远的距离显示
    zIndex:0, // 设置当前线的显示层级，只有在clampToGround 属性为true的时候才生效
    },
 // 添加线体
polylineVolume:{
    show:true|false, // 是否展示线
    positions:Cesium.Cartesian3.fromDegreesArray([123.91264, 41.86205, 123.91264, 40.86209]),
    shape: [
      new Cesium.Cartesian2(-50000, -50000),
      new Cesium.Cartesian2(50000, -50000),
      new Cesium.Cartesian2(50000, 50000),
      new Cesium.Cartesian2(-50000, 50000),
    ],
 // 挤出来的数组
    cornerType:CornerType.ROUNDED, // 指定角的样式
    granularity:Cesium.Math.RADIANS_PER_DEGRE, // 指定经纬度之间角的距离
    fill:true|false, // 是否填充
    material:Color.WHITE, // 填充的材质或者是填充的颜色
    outline:true|false, // 是否显示外边框
    outlineColor:Color.BLACK, // 外边框的颜色
    outlineWidth:1.0, // 填充线的宽度
    shadows:ShadowMode.DISABLED, // 设置阴影
},
// 添加墙
wall:{
    show:true|false, // 是否展示墙

}

})
```

### Cesium 结合 Echarts 
```javascript
// 1 导入 echarts （不能使用echarts模块，导入的是echartsjs文件）
import echarts from '@/assets/cesiumModel/echarts.min.js';
import { viewer, Cesium } from '@/assets/cesiumModel/index';

function myCesiumEcharts(o) {
    // 这个函数是用来解决cesium和echarts的方法(这里是固定的....)
    (function (e) {
        const t = {};
        function n(r) {
            if (t[r]) return t[r].exports;
            const i = t[r] = {
                i: r,
                l: !1,
                exports: {}
            };
            return e[r].call(i.exports, i, i.exports, n),
                i.l = !0,
                i.exports
        }
        n.m = e,
            n.c = t,
            n.d = function (e, t, r) {
                n.o(e, t) || Object.defineProperty(e, t, {
                    enumerable: !0,
                    get: r
                })
            },
            n.r = function (e) {
                "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
                    value: "Module"
                }),
                    Object.defineProperty(e, "__esModule", {
                        value: !0
                    })
            },
            n.t = function (e, t) {
                if (1 & t && (e = n(e)), 8 & t) return e;
                if (4 & t && "object" == typeof e && e && e.__esModule) return e;
                const r = Object.create(null);
                if (n.r(r), Object.defineProperty(r, "default", {
                    enumerable: !0,
                    value: e
                }), 2 & t && "string" != typeof e) for (let i in e) n.d(r, i,
                    function (t) {
                        return e[t]
                    }.bind(null, i));
                return r
            },
            n.n = function (e) {
                let t = e && e.__esModule ?
                    function () {
                        return e.
                            default
                    } :
                    function () {
                        return e
                    };
                return n.d(t, "a", t),
                    t
            },
            n.o = function (e, t) {
                return Object.prototype.hasOwnProperty.call(e, t)
            },
            n.p = "",
            n(n.s = 0)
    })([function (e, t, n) { e.exports = n(1) }, function (e, t, n) {
        echarts ? n(2).load() : console.error("没有找到echarts插件！！！")
    }, function (e, t, n) {
        "use strict";
        function r(e, t) {
            for (let n = 0; n < t.length; n++) {
                let r = t[n];
                r.enumerable = r.enumerable || !1,
                    r.configurable = !0,
                    "value" in r && (r.writable = !0),
                    Object.defineProperty(e, r.key, r)
            }
        }
        n.r(t);
        echarts.cesiumViewer = this.viewer
        let i = function () {
            function e(t, n) {
                !
                    function (e, t) {
                        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
                    }(this, e),
                    this._viewer = t,
                    this.dimensions = ["lng", "lat"],
                    this._mapOffset = [0, 0],
                    this._api = n
            }
            let t, n, i;
            return t = e,
                i = [{
                    key: "create",
                    value: function (t, n) {
                        let r;
                        t.eachComponent("GLMap",
                            function (t) {
                                (r = new e(echarts.cesiumViewer, n)).setMapOffset(t.__mapOffset || [0, 0]),
                                    t.coordinateSystem = r
                            }),
                            t.eachSeries(function (e) {
                                "GLMap" === e.get("coordinateSystem") && (e.coordinateSystem = r)
                            })
                    }
                },
                {
                    key: "dimensions",
                    get: function () {
                        return ["lng", "lat"]
                    }
                }],
                (n = [{
                    key: "setMapOffset",
                    value: function (e) {
                        return this._mapOffset = e,
                            this
                    }
                },
                {
                    key: "getViewer",
                    value: function () {
                        return this._viewer
                    }
                },
                {
                    key: "dataToPoint",
                    value: function (e) {
                        let t = this._viewer.scene,
                            n = [0, 0],
                            r = Cesium.Cartesian3.fromDegrees(e[0], e[1]);
                        if (!r) return n;
                        if (t.mode === Cesium.SceneMode.SCENE3D && Cesium.Cartesian3.angleBetween(t.camera.position, r) > Cesium.Math.toRadians(80)) return !1;
                        let i = t.cartesianToCanvasCoordinates(r);
                        return i ? [i.x - this._mapOffset[0], i.y - this._mapOffset[1]] : n
                    }
                },
                {
                    key: "pointToData",
                    value: function (e) {
                        let t = this._mapOffset,
                            n = viewer.scene.globe.ellipsoid,
                            r = new Cesium.cartesian3(e[1] + t, e[2] + t[2], 0),
                            i = n.cartesianToCartographic(r);
                        return [i.lng, i.lat]
                    }
                },
                {
                    key: "getViewRect",
                    value: function () {
                        let e = this._api;
                        return new echarts.graphic.BoundingRect(0, 0, e.getWidth(), e.getHeight())
                    }
                },
                {
                    key: "getRoamTransform",
                    value: function () {
                        return echarts.matrix.create()
                    }
                }]) && r(t.prototype, n),
                i && r(t, i),
                e
        }();
        echarts.extendComponentModel({
            type: "GLMap",
            getViewer: function () {
                return echarts.cesiumViewer
            },
            defaultOption: {
                roam: !1
            }
        }),
            echarts.extendComponentView({
                type: "GLMap",
                init: function (e, t) {
                    this.api = t,
                        echarts.cesiumViewer.scene.postRender.addEventListener(this.moveHandler, this)
                },
                moveHandler: function (e, t) {
                    this.api.dispatchAction({
                        type: "GLMapRoam"
                    })
                },
                render: function (e, t, n) { },
                dispose: function (e) {
                    echarts.cesiumViewer.scene.postRender.removeEventListener(this.moveHandler, this)
                }
            });
        function a() {
            echarts.registerCoordinateSystem("GLMap", i),
                echarts.registerAction({
                    type: "GLMapRoam",
                    event: "GLMapRoam",
                    update: "updateLayout"
                },
                    function (e, t) { })
        }
        n.d(t, "load",
            function () {
                return a
            })
    }])
    echarts.cesiumViewer = viewer

    // 开始使用echarts
    /** 基本思路
     * 1. 创建div，用来放置echarts
     * 2. 创建在echarts中放置定义好数据的echarts
     * 3. 将div放在地图上
    */
    //  重点代码 CesiumEcharts(viewer,echarts需要渲染的数据)
    function CesiumEcharts(viewer, option) {
        removeEcharts()
        // 获取到渲染Cesium的父节点元素
        let CesiumParentNode = viewer.container;

        // 1. 创建一个空的div，来放置eharts
        let div = document.createElement('div');
        div.style.position = 'absolute';
        div.style.top = '0px';
        div.style.left = '0px';

        div.style.width = viewer.scene.canvas.width + 'px';
        div.style.height = viewer.scene.canvas.height + 'px';
        div.style.pointerEvents = 'none'; // ** 设置鼠标无法选中

        // 定义一个当前渲染echarts的数量(主要是防止生成的随机数冲突，这样可以设置最起码可以渲染10多个echarts)
        let divLength = document.getElementsByClassName('div_Cesium_echarts').length;

        let divID = `Cesium-echarts-${mathRandom(0, 100000)}-${Number(divLength)}`
        div.setAttribute('class', 'div_Cesium_echarts');
        div.setAttribute('id', divID)

        // 将定义好的div节点渲染到cesium父节点下
        CesiumParentNode.appendChild(div);

        // 2. 实例化echarts // 3. 将数据放在页面上
        echarts.init(document.getElementById(divID)).setOption(option)

    }

    // 生成一个随机数（不重要-主要是为了生成echarts id 此处（函数）可以忽略）
    function mathRandom(m, n) {
        return parseInt(Math.random() * (n - m + 1) + m);
    }

    // 销毁echarts
    function removeEcharts() {
        let echartsDiv = document.getElementsByClassName('div_Cesium_echarts');
        for (let i = 0; i < echartsDiv.length; i++) {
            let parentNode = echartsDiv[i].parentNode;
            parentNode.removeChild(echartsDiv[i])
        }
    }
    CesiumEcharts(viewer, o)
}
export {
    myCesiumEcharts
}
```