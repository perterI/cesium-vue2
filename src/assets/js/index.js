// 初始化 cesium 地图

let viewer = null;

function CreateCesium() {
    const china = Cesium.Rectangle.fromDegrees(100, 10, 120, 70);
    Cesium.Camera.DEFAULT_VIEW_RECTANGLE = china;// 将默认视角设置为中国

    Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJkMTkzNWM4OS02NzBiLTQwZGYtYjI3My01ZTJkNzQ2OWJiNmIiLCJpZCI6MTAwNDMxLCJpYXQiOjE2NTcyMDQ3NDl9.0jgC5ffeJysmGxRaNiweoxbZcIR-eeqdQVaTFuzP1s8';
    viewer = new Cesium.Viewer(id, {
        animation: false, //是否显示动画控件
        homeButton: false, //是否显示home键
        geocoder: true, //是否显示地名查找控件        如果设置为true，则无法查询
        baseLayerPicker: false, //是否显示图层选择控件
        timeline: false, //是否显示时间线控件
        fullscreenButton: false, //是否全屏显示
        scene3DOnly: false, //如果设置为true，则所有几何图形以3D模式绘制以节约GPU资源
        infoBox: true, //是否显示点击要素之后显示的信息
        sceneModePicker: false, //是否显示投影方式控件  三维/二维
        navigationInstructionsInitiallyVisible: true,
        navigationHelpButton: false, //是否显示帮助信息控件
        selectionIndicator: true, //是否显示指示器组件
        shouldAnimate: true,
        vrButton: false,
        terrainProvider: new Cesium.CesiumTerrainProvider({
            url: Cesium.IonResource.fromAssetId(1),
        }),
    });
    // 去处cesium logo
    viewer.cesiumWidget.creditContainer.style.display = "none";
    viewer.scene.debugShowFramesPerSecond = true;
}

