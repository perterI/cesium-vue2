// 用来绑定cesium中的一些属性和方法，触发事件
;
import route from "../shuju";
import axios from 'axios'
let Cesium = window.Cesium
let viewer = null;
function createViewer(id) {
    const china = Cesium.Rectangle.fromDegrees(100, 10, 120, 70);
    Cesium.Camera.DEFAULT_VIEW_RECTANGLE = china;// 将默认视角设置为中国

    Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJkMTkzNWM4OS02NzBiLTQwZGYtYjI3My01ZTJkNzQ2OWJiNmIiLCJpZCI6MTAwNDMxLCJpYXQiOjE2NTcyMDQ3NDl9.0jgC5ffeJysmGxRaNiweoxbZcIR-eeqdQVaTFuzP1s8';
    viewer = new Cesium.Viewer(id, {
        animation: true, //是否显示动画控件
        homeButton: false, //是否显示home键
        geocoder: true, //是否显示地名查找控件        如果设置为true，则无法查询
        baseLayerPicker: false, //是否显示图层选择控件
        timeline: true, //是否显示时间线控件
        fullscreenButton: false, //是否全屏显示
        scene3DOnly: true, //如果设置为true，则所有几何图形以3D模式绘制以节约GPU资源
        infoBox: true, //是否显示点击要素之后显示的信息
        sceneModePicker: false, //是否显示投影方式控件  三维/二维
        navigationInstructionsInitiallyVisible: true,
        navigationHelpButton: false, //是否显示帮助信息控件
        selectionIndicator: true, //是否显示指示器组件
        shouldAnimate: true,
        vrButton: false,
        terrainProvider: new Cesium.CesiumTerrainProvider({
            url: Cesium.IonResource.fromAssetId(1),
        }),
        useBrowserRecommendedResolution: true
    });
    // 去处cesium logo
    viewer.cesiumWidget.creditContainer.style.display = "none";
    viewer.scene.debugShowFramesPerSecond = true;
    viewer.scene.globe.depthTestAgainstTerrain = false
}
// 相机的飞行事件
function flyTo() {
    viewer.flyTo({
        // x y z 
        destination: Cesium.Cartesian3.fromDegrees(-117.16, 32.71, 150000.0)
    });
}
function cameraFlyto(x, y, height) {
    viewer.camera.flyTo({
        destination: Cesium.Cartesian3.fromDegrees(x, y, height)
    })
}
function createPoint(x, y, height = 0) {
    viewer.entities.add({
        position: Cesium.Cartesian3.fromDegrees(x, y, height),
        point: {
            pixelSize: 10, // 当前点的大小
            color: Cesium.Color.RED, // 当然是颜色
        },
    });

}
async function controlCamera() {
    const resource = await Cesium.IonResource.fromAssetId(1204516);
    var position = Cesium.Cartesian3.fromDegrees(-123.0744619, 44.0503706, 50000)
    let pitch = Cesium.Math.toRadians(0); // 模型头部向上的角度
    let roll = Cesium.Math.toRadians(0);
    let angle = 0
    // setInterval(function () {
    viewer.entities.removeAll();
    angle += 10;
    let heading = Cesium.Math.toRadians(angle);
    let hpr = new Cesium.HeadingPitchRoll(heading, pitch, roll)
    // 第一个参数是笛卡尔坐标 第二个参数就是需要展示的角度
    let orientation = Cesium.Transforms.headingPitchRollQuaternion(position, hpr);
    function getColor(colorName, alpha) {
        const color = Cesium.Color[colorName.toUpperCase()];
        return Cesium.Color.fromAlpha(color, parseFloat(alpha));
    }
    // Cesium.Color.fromAlpha(Cesium.Color[颜色名称],parseFloat(透明值))
    console.log(getColor('RED', 0));
    const entity = viewer.entities.add({
        position: position,
        orientation,
        model: {
            uri: resource,
            minimumPixelSize: 128,
            maximumScale: 20000,
            color: getColor('RED', 0),
        },
    });
    viewer.flyTo(entity)

    // }, 500)



    // viewer.camera.lookUp(Cesium.Math.toRadians(10)) // 相机向上旋转多少度

    // console.log(hpr);
    // viewer.camera.lookDown(Cesium.Math.toRadians(10)) // 相机向下旋转多少度
    // viewer.camera.lookLeft(Cesium.Math.toRadians(10))// 相机向左旋转多少度
    // viewer.camera.lookRight(Cesium.Math.toRadians(10)) // 相机向右旋转多少度

    // viewer.camera.moveUp(100)// 相机向前平移
    // viewer.camera.moveDown(100)// 相机整体向后平移
    // viewer.camera.moveLeft(100) // 相机整体向左
    // viewer.camera.moveRight(100) // 相机整体向右平移
    // viewer.camera.moveForward(100) // 相机向正前方移动

    // viewer.camera.twistLeft(Cesium.Math.toRadians(10)) // 整个地球的旋转（左）
    // viewer.camera.twistRight(Cesium.Math.toRadians(10))// 整个地球的旋转（右）

    // viewer.camera.zoomIn(10)//相机向前移动，对焦
    // viewer.camera.zoomOut(10)// 相机向后移动，对焦

}
// 回到默认地址
function goHome() {
    viewer.camera.flyHome(1)
}
// 生成随机数，x-y
function RandomNum(x, y) {
    return Math.round(Math.random() * (y - x) + x)
}
// 清除绘制
function removeDataSources() {
    viewer.dataSources.removeAll(); // 清除所有的信息
    if (viewer.entities) {
        viewer.entities.removeAll();
    }
}
/*
默认样式 default 
基础样式 basics 
标记标签 sign 
分色渲染 colorSeparation  
标签加分色渲染 tableRender
渐变 gradient
高度可视化 heightVisualization
*/
function addGeoJson(type = 'default') {
    removeDataSources(); // 清除地图上的点位和绘制的一些信息
    let data = null;
    if (type === 'basics' || type === 'sign') {
        data = {
            fill: Cesium.Color.PINK.withAlpha(0.5),
        }
    }
    var promise = Cesium.GeoJsonDataSource.load('json/dizhou.geojson', data);// 读取文件，返回一个promise对象
    viewer.flyTo(promise)
    let colorHash = {}
    promise.then(function (data) {
        let entities = data.entities.values; // 所有的点位

        for (var i = 0; i < entities.length; i++) {
            var entity = entities[i];
            var name = entity.name;
            let color = colorHash[name];
            entity.polygon.name = name;
            if (type === 'colorSeparation' || type === 'tableRender' || type === 'gradient' || type === 'heightVisualization') {
                if (!color) {
                    color = Cesium.Color.fromRandom({
                        alpha: type === 'gradient' ? 0.4 : 1,
                    });
                    colorHash[name] = color;
                }
                entity.polygon.material = color;
                entity.polygon.outline = false;
            }

            if (type === 'sign' || type === 'tableRender' || type === 'gradient') {

                // 添加标签 start
                var polyPostions = entity.polygon.hierarchy.getValue(Cesium.JulianDate.now()).positions; //从多边形上取出他的顶点
                var polyCenter = Cesium.BoundingSphere.fromPoints(polyPostions).center;  //通过顶点构建一个包围球
                polyCenter = Cesium.Ellipsoid.WGS84.scaleToGeodeticSurface(polyCenter);   //把包围球得中心做贴地得偏移
                entity.position = polyCenter; // 获取到当前地址的中心点位，并且是笛卡尔3d坐标
                viewer.entities.add({
                    name: name,
                    position: polyCenter,
                    label: {
                        id: entity.id,
                        text: name, // 展示的内容
                        showBackground: false, // 是否展示背景颜色
                        scale: .7, // 放大或缩小的比例
                        horizontalOrigin: Cesium.HorizontalOrigin.CENTER, // 标签的位置
                        font: '16px 微软雅黑',// 标签的字体大小
                    }
                });
                // 添加标签end
            }
            entity.polygon.extrudedHeight = type === 'default' ? 0 : type === 'heightVisualization' ? RandomNum(500, 500000) : 5000.0
        }

        viewer.dataSources.add(data);

    })
}
// 清除飞机
function destroyPlane() {
    // viewer.entities?.removeAll();
}

async function planeTracker() {
    destroyPlane()
    const resource = await Cesium.IonResource.fromAssetId(1204516); // 请求飞机模型
    // 路线数据 数组对象格式 对象中有longitude latitude height 属性
    // 飞机位置的经度纬度和高度
    const { data: { data: data } } = await axios.post('https://www.fastmock.site/mock/abd498cd4c81472dd2a1ff80af4592b2/cesium/planeRouter')
    const flightData = data

    const timeStepInSeconds = 30;
    const totalSeconds = timeStepInSeconds * (flightData.length - 1); // 设置飞机飞行总的时长
    // console.log(totalSeconds,'totalSeconds');
    const start = Cesium.JulianDate.fromIso8601("2020-03-09T23:10:00Z");// 设置开始时间
    // 添加时间线上 
    const stop = Cesium.JulianDate.addSeconds(start, totalSeconds, new Cesium.JulianDate());
    console.log(stop, '这是没有克隆的结束时间');

    console.log(stop.clone(), '这是克隆的结束时间');
    viewer.clock.startTime = start.clone();
    viewer.clock.stopTime = stop.clone();
    viewer.clock.currentTime = start.clone();

    // viewer.timeline.zoomTo(start, stop); // 将时间线转换
    viewer.clock.clockRange = Cesium.ClockRange.LOOP_STOP;

    viewer.clock.multiplier = 70;// 设置时间表的速度

    viewer.clock.shouldAnimate = false;// 默认是否进入动画

    // SampledPositionedProperty存储雷达样本序列中每个样本的位置和时间戳
    const positionProperty = new Cesium.SampledPositionProperty();
    // console.log(positionProperty);
    for (let i = 0; i < flightData.length; i++) {
        const dataPoint = flightData[i];

        const time = Cesium.JulianDate.addSeconds(start, i * timeStepInSeconds, new Cesium.JulianDate());
        const position = Cesium.Cartesian3.fromDegrees(dataPoint.longitude, dataPoint.latitude, dataPoint.height);

        positionProperty.addSample(time, position);
        // console.log(positionProperty,'当前总的时间段和坐标');
        // console.log('当前的时间：', time + ' 当前的地址：' + position);
        // 循环 - 绘制红色的点位
        viewer.entities.add({
            show: false,
            name: '点',
            description: `当前点的位置信息: (${dataPoint.longitude}, ${dataPoint.latitude}, ${dataPoint.height})`,
            position: position,
            point: { pixelSize: 5, color: Cesium.Color.PINK }
        });
    }
    // 创建一个实体，用一条线显示整个雷达样本序列，并添加一个沿其移动的点the samples.
    // 添加飞行物体

    const airplaneEntity = viewer.entities.add({
        name: '我是飞机模型',
        // description: `当前点的位置信息: (${positionProperty.longitude}, ${positionProperty.latitude}, ${positionProperty.height})`,
        availability: new Cesium.TimeIntervalCollection([new Cesium.TimeInterval({ start: start, stop: stop })]),
        position: positionProperty,
        orientation: new Cesium.VelocityOrientationProperty(positionProperty), // 让当前的飞机模型的角度动态的变化
        // 飞机模型属性
        model: {
            show: true,
            uri: resource,
            scale: 1,
            minimumPixelSize: 128, // 最小大小
            maximumScale: 5200, // 最大大小
        },
        // 飞机路线 黄色的线条
        path: {
            show: false,
            resolution: 1,
            material: new Cesium.PolylineGlowMaterialProperty({
                glowPower: 0.1,
                color: Cesium.Color.YELLOW,
            }),
            width: 1,
        }
    });
    // 将摄像机定位到飞行的物体上面
    viewer.trackedEntity = airplaneEntity;

}
// 控制当前时间的开始和暂停
function startClock() {
    viewer.clock.shouldAnimate = !viewer.clock.shouldAnimate
    return false
}
var alreadySpeed = true;

function doubleSpeed() {
    if (alreadySpeed) {
        alreadySpeed = false;
        viewer.clock.multiplier = viewer.clock.multiplier * 2
    } else {
        viewer.clock.multiplier = 70
    }
}


function createPlane2() {

    //Enable lighting based on the sun position
    viewer.scene.globe.enableLighting = true;

    //Enable depth testing so things behind the terrain disappear.
    viewer.scene.globe.depthTestAgainstTerrain = true;

    //Set the random number seed for consistent results.
    Cesium.Math.setRandomNumberSeed(3);

    //Set bounds of our simulation time
    const start = Cesium.JulianDate.fromDate(new Date(2015, 2, 25, 16));
    const stop = Cesium.JulianDate.addSeconds(
        start,
        360,
        new Cesium.JulianDate()
    );

    //Make sure viewer is at the desired time.
    viewer.clock.startTime = start.clone();
    viewer.clock.stopTime = stop.clone();
    viewer.clock.currentTime = start.clone();
    viewer.clock.clockRange = Cesium.ClockRange.LOOP_STOP; //Loop at the end
    viewer.clock.multiplier = 10;

    //Set timeline to simulation bounds
    viewer.timeline.zoomTo(start, stop);

    //Generate a random circular pattern with varying heights.
    function computeCirclularFlight(lon, lat, radius) {
        const property = new Cesium.SampledPositionProperty();
        for (let i = 0; i <= 360; i += 45) {
            const radians = Cesium.Math.toRadians(i);
            const time = Cesium.JulianDate.addSeconds(
                start,
                i,
                new Cesium.JulianDate()
            );
            const position = Cesium.Cartesian3.fromDegrees(
                lon + radius * 1.5 * Math.cos(radians),
                lat + radius * Math.sin(radians),
                Cesium.Math.nextRandomNumber() * 500 + 1750
            );
            property.addSample(time, position);

            //Also create a point for each sample we generate.
            viewer.entities.add({
                position: position,
                point: {
                    pixelSize: 3,
                    color: Cesium.Color.TRANSPARENT,
                    outlineColor: Cesium.Color.YELLOW,
                    outlineWidth: 2,
                },
            });
        }
        return property;
    }

    //Compute the entity position property.
    const position = computeCirclularFlight(-112.110693, 36.0994841, 0.03);

    //Actually create the entity
    const entity = viewer.entities.add({
        //Set the entity availability to the same interval as the simulation time.
        availability: new Cesium.TimeIntervalCollection([
            new Cesium.TimeInterval({
                start: start,
                stop: stop,
            }),
        ]),

        //Use our computed positions
        position: position,

        // 将模型对正线的角度
        orientation: new Cesium.VelocityOrientationProperty(position),

        //Load the Cesium plane model to represent the entity
        model: {
            uri: "../SampleData/models/CesiumAir/Cesium_Air.glb",
            minimumPixelSize: 64,
        },

        //
        path: {
            resolution: 1,
            material: new Cesium.PolylineGlowMaterialProperty({
                glowPower: 0.1,
                color: Cesium.Color.YELLOW,
            }),
            width: 10,
        },
    });

    //Add button to view the path from the top down
    Sandcastle.addDefaultToolbarButton("View Top Down", function () {
        viewer.trackedEntity = undefined;
        viewer.zoomTo(
            viewer.entities,
            new Cesium.HeadingPitchRange(0, Cesium.Math.toRadians(-90))
        );
    });

    //Add button to view the path from the side
    Sandcastle.addToolbarButton("View Side", function () {
        viewer.trackedEntity = undefined;
        viewer.zoomTo(
            viewer.entities,
            new Cesium.HeadingPitchRange(
                Cesium.Math.toRadians(-90),
                Cesium.Math.toRadians(-15),
                7500
            )
        );
    });

    //Add button to track the entity as it moves
    Sandcastle.addToolbarButton("View Aircraft", function () {
        viewer.trackedEntity = entity;
    });

    //Add a combo box for selecting each interpolation mode.
    Sandcastle.addToolbarMenu(
        [
            {
                text: "Interpolation: Linear Approximation",
                onselect: function () {
                    entity.position.setInterpolationOptions({
                        interpolationDegree: 1,
                        interpolationAlgorithm: Cesium.LinearApproximation,
                    });
                },
            },
            {
                text: "Interpolation: Lagrange Polynomial Approximation",
                onselect: function () {
                    entity.position.setInterpolationOptions({
                        interpolationDegree: 5,
                        interpolationAlgorithm:
                            Cesium.LagrangePolynomialApproximation,
                    });
                },
            },
            {
                text: "Interpolation: Hermite Polynomial Approximation",
                onselect: function () {
                    entity.position.setInterpolationOptions({
                        interpolationDegree: 2,
                        interpolationAlgorithm: Cesium.HermitePolynomialApproximation,
                    });
                },
            },
        ],
        "interpolationMenu"
    );

}

function createRoute() {
    console.log(route);

}
// 多个点的连接
import logo from '@/assets/logo.png'
function connectPoints() {
    // console.log(Cesium.Color.YELLOW,'颜色');

    // let img = 'https://www.baidu.com/img/flexible/logo/pc/result@2.png'
    let entitiesPoint1 = viewer.entities.add({
        position: Cesium.Cartesian3.fromDegrees(123.91264, 41.86205),
        point: {
            show: false,
            pixelSize: 10,
            color: Cesium.Color.YELLOW,
        },
        billboard: {
            image: logo,
            horizontalOrigin: Cesium.HorizontalOrigin.RIGHT,
            verticalOrigin: Cesium.VerticalOrigin.LEFT,
            width: 20,// 设置 billboard（图片） 的宽
            height: 20, // 设置 billboard (图片) 的高
            sizeInMeters: true
        },
        label: {
            text: '我是vue图标',
            font: '12px 微软雅黑',
            style: "FILL",
            showBackground: true,
            pixelOffset: new Cesium.Cartesian2(8, 0), // 当前label的偏移量
            totalScale: .5,
            horizontalOrigin: Cesium.HorizontalOrigin.LEFT, // 当前label的位置
            translucencyByDistance: new Cesium.NearFarScalar(
                1.5e2,
                1.0,
                1.5e8,
                0.0
            ),
        }
    })
    setTimeout(() => {
        entitiesPoint1.label.font = '20px 楷体'
    }, 3000);
    // console.log(entitiesPoint1.label.font);
    // let outlineDelta = 0.1;
    // let fontDelta = -1.0;
    // let fontSize = 20.0;
    // const minFontSize = 1.0;
    // const maxFontSize = 48.0;

    //    let labelListenerCallback = viewer.scene.preUpdate.addEventListener(
    //         function (scene, time) {
    //             entity.label.outlineWidth += outlineDelta;
    //             if (
    //                 entity.label.outlineWidth >= 4.0 ||
    //                 entity.label.outlineWidth <= 0.0
    //             ) {
    //                 outlineDelta *= -1.0;
    //             }

    //             fontSize += fontDelta;
    //             if (fontSize >= maxFontSize || fontSize <= minFontSize) {
    //                 fontDelta *= -1.0;
    //             }
    //             entity.label.font = `${fontSize}px Calibri`;
    //         })
    // console.log(entitiesPoint1,'第一个点的信息');
    console.log(Cesium.Cartographic(123.91264, 41.86205, 100));
    let entitiesPoint2 = viewer.entities.add({
        position: Cesium.Cartesian3.fromDegrees(123.91264, 40.86209),
        point: {
            pixelSize: 10,
            color: Cesium.Color.YELLOW,
            scaleByDistance: new Cesium.NearFarScalar(1.5e2, 2.0, 1.5e7, 0.5),
        },
    })

    // let p1 = new Cesium.Cartographic(123.91264, 41.86205,0)
    // let p2 = new Cesium.Cartographic(123.91264, 40.86209,0)

    // console.log(new Cesium.EllipsoidGeodesic().setEndPoints(p1,p2));
    viewer.entities.add({
        name: "这是一个有高度的线",
        polyline: {
            positions: Cesium.Cartesian3.fromDegreesArrayHeights([123.91264, 41.86205, 10000, 123.91264, 40.86209, 10000, 124.91264, 41.86205, 10000]),
            width: 10,
            material: new Cesium.PolylineGlowMaterialProperty({
                glowPower: 0.2,
                taperPower: 0.5,
                color: Cesium.Color.CORNFLOWERBLUE,
            }),
        },
    });
    viewer.flyTo(entitiesPoint1)
}

// import lnInfo from '@/assets/sim-data/link_board.json'
// 添加辽宁省信息
async function loadLnprovince() {

    let Lnprovince = await axios.get('https://www.fastmock.site/mock/abd498cd4c81472dd2a1ff80af4592b2/cesium/lnData')
    let lnInfo = await axios.get('https://www.fastmock.site/mock/abd498cd4c81472dd2a1ff80af4592b2/cesium/lnInfo')


    var p = Cesium.GeoJsonDataSource.load(Lnprovince.data, {})
    viewer.flyTo(p) // 飞行至辽宁省
    p.then(function (res) {
        viewer.dataSources.add(res);
    })

    let boardList = lnInfo.data.boardList;
    for (let i = 0; i < boardList.length; i++) {

        viewer.entities.add({
            name: boardList[i].name,

            position: Cesium.Cartesian3.fromDegrees(boardList[i].position[0], boardList[i].position[1]),
            point: {
                pixelSize: 10, // 当前点的大小
                color: Cesium.Color.PINK, // 当然是颜色
            },
            label: {
                text: boardList[i].name,// 显示的文本内容
                font: '12px 微软雅黑', // 字体设置
                showBackground: true, // 是否显示背景颜色
                horizontalOrigin: Cesium.HorizontalOrigin.LEFT, // label 的显示位置
            }
        })
    }

    let line1 = lnInfo.data.line1;
    let lineArr = [];

    for (let j = 0; j < line1.length; j++) {
        lineArr.push(line1[j].position)
    }

    console.log(lineArr.flat());
    addPointLine('line1', lineArr.flat())
}

function addPointLine(name, polylineArray) {
    viewer.entities.add({
        name,
        polyline: {
            width: 20,
            positions: Cesium.Cartesian3.fromDegreesArray(polylineArray),
            material: new Cesium.PolylineGlowMaterialProperty({
                color: Cesium.Color.CORNFLOWERBLUE,
            }),
        }
    })

}

async function createCityModule() {
    const resource = await Cesium.IonResource.fromAssetId(1213019);
    const entity = viewer.entities.add({
        position: Cesium.Cartesian3.fromDegrees(123.91264, 40.86209, 0),
        model: {
            uri: resource,
            minimumPixelSize: 128,
            maximumScale: 20000,
        },
    });
    viewer.clock.shouldAnimate = true;
    viewer.flyTo(entity)
}
// 从北京到太原的飞机路线
import vueLogo from '@/assets/logo.png'

async function router1() {
    const resource = await Cesium.IonResource.fromAssetId(1213022);  // 飞机模型
    // 路线数据
    let flightData = [{
        longitude: 116.47952546093748, latitude: 39.7287854073587, height: 0
    }, {
        longitude: 115.7883816015625, latitude: 40.14156954139767, height: 100
    }, {
        longitude: 115.6345730078125, latitude: 40.17515539960071, height: 10000
    }, {
        longitude: 113.0637722265625, latitude: 38.475585288062604, height: 10000
    }, {
        longitude: 112.48699, latitude: 37.94036, height: 0
    },]
    // 总的时间长度
    let allTimerLong = 30 * (flightData.length - 1);
    // 设置钟表的开始时间    
    let ClockStartTimer = Cesium.JulianDate.fromIso8601("2022-07-21T23:10:00Z");
    // 设置钟表的结束时间   
    let ClockEndTimer = Cesium.JulianDate.addSeconds(ClockStartTimer, allTimerLong, new Cesium.JulianDate())

    // 设置钟表的一些设置 开始时间，结束时间，当前时间，...
    viewer.clock.startTime = ClockStartTimer;
    viewer.clock.currentTime = ClockStartTimer;
    viewer.clock.stopTime = ClockEndTimer;
    viewer.clock.clockRange = Cesium.ClockRange.LOOP_STOP;
    viewer.clock.clockStep = Cesium.ClockStep.SYSTEM_CLOCK_MULTIPLIER
    viewer.clock.multiplier = 30
    viewer.clock.shouldAnimate = false

    const positionProperty = new Cesium.SampledPositionProperty();
    // 循环路线中的每个点位
    for (let i = 0; i < flightData.length; i++) {
        let pointData = flightData[i];
        // 定义一个时间段
        const time = Cesium.JulianDate.addSeconds(ClockStartTimer, i * 30, new Cesium.JulianDate());
        // 找到数组里面的各个点位
        const position = Cesium.Cartesian3.fromDegrees(pointData.longitude, pointData.latitude, pointData.height);
        // 添加一个样本
        positionProperty.addSample(time, position)
    }
    console.log('当前飞机的角度：', new Cesium.VelocityOrientationProperty(positionProperty));
    let plane1 = viewer.entities.add({
        name: "北京 -> 太原",
        orientation: new Cesium.VelocityOrientationProperty(positionProperty), // 让当前的飞机模型的角度动态的变化
        availability: new Cesium.TimeIntervalCollection([new Cesium.TimeInterval({ start: ClockStartTimer, stop: ClockEndTimer })]),
        position: positionProperty,
        model: {
            uri: resource, // 当前的飞机模型
            minimumPixelSize: 128,
            maximumScale: 3200,
        },
        label:{
            text:'我是飞行的模型'
        },
        // billboard:{
        //     image:vueLogo,
        //     width:20,
        //     height:20
        // },
        path: {
            show: true,
            resolution: 1,
            material: new Cesium.PolylineGlowMaterialProperty({
                glowPower: 0.1,
                color: Cesium.Color.YELLOW,
            }),
            width: 1,
            leadTime: 0,
            trailTime: 0,
        }
    })
    viewer.trackedEntity = plane1
}

// 检测两个点的距离
function disTance() {
    /* var satrt = Cesium.Cartographic.fromDegrees(117.270739, 31.84309)
    console.log(satrt, '开始点');
    var end = Cesium.Cartographic.fromDegrees(117.270739, 31.85309)
    console.log(end, '结束点');
    var geodesic = new Cesium.EllipsoidGeodesic();
    geodesic.setEndPoints(satrt, end);
    var distance = geodesic.surfaceDistance
    return { distance, geodesic } */
    let p1 = Cesium.Cartesian3.fromDegrees(113.65, 23.33, 500);
    let p2 = Cesium.Cartesian3.fromDegrees(113.65, 23.33, 10);

    viewer.entities.add({
        name: '这是第一个点的信息',
        position: p1,
        point: {
            pixelSize: 10, // 当前点的大小
            color: Cesium.Color.YELLOW, // 当然是颜色
        },
    })
    viewer.entities.add({
        name: '这是第二个点的信息',
        position: p2,
        point: {
            pixelSize: 10, // 当前点的大小
            color: Cesium.Color.RED, // 当然是颜色
        },
    })
    var startPosition = p1

    var endPosition = p2

    var distance = Cesium.Cartesian3.distance(startPosition, endPosition)
    viewer.camera.flyTo({
        destination: p1
    })
    console.log("当前两个点的距离（m）：", (Math.floor(distance * 100)) / 100);

}
function createBillboards() {
    const billboards = viewer.scene.primitives.add(new Cesium.BillboardCollection());
    billboards.add({
        name: 'aaa',
        position: new Cesium.Cartesian3(112.0, 42.0, 11113.0),
        image: logo,
        width: 120,
        height: 120,

    });
    billboards.add({
        position: new Cesium.Cartesian3(4.0, 5.0, 6.0),
        image: logo,
        width: 20,
        height: 20
    });
    // billboards.add({
    //     position: Cesium.Cartesian3.fromDegrees(-75.59777, 40.03883, 150000),
    //     image: "../images/Cesium_Logo_overlay.png",
    //   });
    // viewer.flyTo(billboards)
    viewer.camera.flyTo({
        destination: Cesium.Cartesian3.fromDegrees(112.0, 42.0, 11113.0)
    })
}


export {
    viewer,
    createViewer,
    flyTo,
    addGeoJson,
    RandomNum,
    removeDataSources,
    planeTracker,
    destroyPlane,
    createRoute,
    connectPoints,
    goHome,
    startClock,
    doubleSpeed,
    cameraFlyto,
    createPoint,
    controlCamera,
    loadLnprovince,
    addPointLine,
    createCityModule,
    router1,
    disTance,
    createBillboards
}