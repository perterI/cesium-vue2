
// ! 关于cesium相机的构造函数
import { viewer } from './cesium'

function CesiumCamera() {
    /** 
     * 飞行至指定位置
     * @param lon 经度
     * @param lat 纬度
     * @param height 距离地面的高度
     * @param time 飞行时间
     * */
    this.flyTo = function (lon, lat, height, time = 1) {
        viewer.camera.flyTo({
            destination: Cesium.Cartesian3.fromDegrees(lon, lat, height), // 飞行位置
            duration: time, // 飞行时间
            // 飞行角度
            orientation: {
                heading: Cesium.Math.toRadians(90),
                pitch: Cesium.Math.toRadians(-90), // 设置俯仰角度
                roll: 0.0
            }
        })
    }
    /** 
     * 切换到指定位置
     * @param lon 经度
     * @param lat 纬度
     * @param height 距离地面的高度
     * */
    this.setView = function (lon, lat, height) {
        viewer.camera.setView({
            destination: Cesium.Cartesian3.fromDegrees(lon, lat, height),
            // 飞行后的方向
            orientation: {
                heading: Cesium.Math.toRadians(0, 0),
                pitch: Cesium.Math.toRadians(-45), // 设置俯仰角度
                roll: 0.0
            }
        })
    }
    /**
     * 相机旋转
     * @param direction 什么方向
     * @param angle 摄像头向上的角度
    */
    this.look = function (direction, angle = 10) {
        let dir = direction.toLowerCase()
        if (dir == 'up') {
            viewer.camera.lookUp(Cesium.Math.toRadians(angle))
        } else if (dir == 'down') {
            viewer.camera.lookDown(Cesium.Math.toRadians(angle))
        } else if (dir == 'left') {
            viewer.camera.lookLeft(Cesium.Math.toRadians(angle))
        } else if (dir == 'right') {
            viewer.camera.lookRight(Cesium.Math.toRadians(angle))
        } else {
            viewer.camera.lookUp(Cesium.Math.toRadians(angle))
        }
    }
    /**
     * 相机移动
     * @param direction 相机移动的方向
     * @param length 相机的移动的距离
     * */
    this.move = function (direction, length = 100) {
        let dir = direction.toLowerCase()
        if (dir == 'up') {
            viewer.camera.moveUp(Cesium.Math.toRadians(length))
        } else if (dir == 'down') {
            viewer.camera.moveDown(Cesium.Math.toRadians(length))
        } else if (dir == 'left') {
            viewer.camera.moveLeft(Cesium.Math.toRadians(length))
        } else if (dir == 'right') {
            viewer.camera.moveRight(Cesium.Math.toRadians(length))
        } else if (dir == 'forward') {
            viewer.camera.moveForward(length)
        } else {
            viewer.camera.moveUp(Cesium.Math.toRadians(length))
        }
    }
    /**
     * 整个地球的旋转
     * @param  direction 方向 left right 
     * @param angle 角度 
     * */
    this.twist = function (direction, angle = 10) {
        let dir = direction.toLowerCase()
        if (dir == 'left') {
            viewer.camera.twistLeft(Cesium.Math.toRadians(angle))
        } else {
            viewer.camera.twistRight(Cesium.Math.toRadians(angle))
        }
    }

    /**
     * 跳转到某个点位上
     * @param flyElement 飞行目标
     * 当flyElement为字符串的时候，当做当前点的来定位，如果为对象，则需要传入经度纬度高度，飞行时间
     * 
     * */
    this.viewFlyto = function (flyElement) {

        viewer.flyTo({
            destination: Cesium.Cartesian3.fromDegrees(flyElement.lon, flyElement.lat, flyElement.height), // 飞行位置
            duration: flyElement.time, // 飞行时间
        })
    }


}

export default new CesiumCamera()