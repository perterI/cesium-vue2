

const Cesium = window.Cesium;
let viewer = null;


function CreateCesium(id) {
    Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJkMTkzNWM4OS02NzBiLTQwZGYtYjI3My01ZTJkNzQ2OWJiNmIiLCJpZCI6MTAwNDMxLCJpYXQiOjE2NTcyMDQ3NDl9.0jgC5ffeJysmGxRaNiweoxbZcIR-eeqdQVaTFuzP1s8'
    viewer = new Cesium.Viewer(id, {
        animation: false, // 是否显示动画表盘
        baseLayerPicker: false, // 是否展示2d3d地图切换
        fullscreenButton: false, //
        geocoder: false,
        homeButton: false,
        infoBox: false,
        sceneModePicker: false,
        selectionIndicator: false,
        timeline: false,
        navigationHelpButton: false,
        scene3DOnly: false,
        terrainProvider: new Cesium.CesiumTerrainProvider({
            url: Cesium.IonResource.fromAssetId(1),
        }),
    })
    viewer._cesiumWidget._creditContainer.style.display = "none";
}

export {
    CreateCesium,
    viewer,
    Cesium
}


