// 放一些关于相机的东西

import { viewer } from './index'
class CesiumCamear {
    // 相机飞行到某个地方
    flyTo(x, y, height = 0, timer = 1.5) {
        viewer.camera.flyTo({
            destination: Cesium.Cartesian3.fromDegrees(x, y, height), // 坐标位置，你要飞行的地点
            duration: timer,// 飞行时间
            // complete, 飞行完成后执行的函数
            // maximumHeight:3000
            orientation: {
                // heading: Cesium.Math.toRadians(0, 0),
                // pitch: Cesium.Math.toRadians(-45), // 设置俯仰角度
                // roll: 0.0
            }
        })
    }
    // 相机切换到某个地方
    setView(x, y, height = 0) {
        viewer.camera.setView({
            destination: Cesium.Cartesian3.fromDegrees(x, y, height),
            // 控制相机的方向
            orientation: {
                heading: Cesium.Math.toRadians(0, 0),
                pitch: Cesium.Math.toRadians(-45), // 设置俯仰角度
                roll: 0.0
            }
        })
    }
    lookUp(){
        viewer.camera.lookUp(Cesium.Math.toRadians(10));// 当前相机向上旋转的度数
    }
    lookDown(){
        viewer.camera.lookDown(Cesium.Math.toRadians(10)); // 当前相机向下旋转的度数
    }
    lookLeft(){
        viewer.camera.lookLeft(Cesium.Math.toRadians(30)); // 当前相机向左旋转的度数
    }
    lookRight(){
        viewer.camera.lookRight(Cesium.Math.toRadians(30)); // 当前相机向右旋转的度数
    }

    moveUp(){
        viewer.camera.moveUp(1000); // 当前相机向前移动的长度
    }
    moveDown(){
        viewer.camera.moveDown(1000);
    }
    moveLeft(){
        viewer.camera.moveLeft(1000);
    }
    moveRight(){
        viewer.camera.moveRight(1000);
    }
    twistLeft(){
        viewer.camera.twistLeft(Cesium.Math.toRadians(10))
    }
    twistRight(){
        viewer.camera.twistRight(Cesium.Math.toRadians(10))
    }

    // 飞行到默认地点
    goHome() {
        viewer.camera.flyHome(1)
    }

}

export default new CesiumCamear()