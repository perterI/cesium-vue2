import { viewer, Cesium } from './index.js'
import echarts from '@/assets/cesiumjs/echarts.min.js'
class Entity {
    addPoint(x, y, h) {
        let point = viewer.entities.add({
            position: Cesium.Cartesian3.fromDegrees(x, y, h), // 添加点的位置信息
            // point 点
            point: {
                show: true,
                pixelSize: 8,
                // color:Cesium.Color.RED,
                outlineColor: Cesium.Color.PINK,
                outlineWidth: 3,
            },
            // billboard : 广告牌
            billboard: {
                show: true,
                image: 'https://cn.vuejs.org/images/logo.svg',
                // pixelOffset:new Cesium.Cartesian2(8, 8), // 图片的 左右 上下 的偏移量
                horizontalOrigin: Cesium.HorizontalOrigin.LEFT, // 水平
                verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
                width: 30,
                height: 30,

            },
            label: {
                show: true,
                text: '世某人',
                font: '12px 微软雅黑',
                style: Cesium.LabelStyle.FILL,
                showBackground: true,
                backgroundColor: Cesium.Color.PINK,
                backgroundPadding: new Cesium.Cartesian2(12, 12),// 设置label的显示背景的大小
                pixelOffset: new Cesium.Cartesian2(25, 0),// 偏移量
                // eyeOffset:new Cesium.Cartesian2(99, 0)
                fillColor: Cesium.Color.RED, // 字体颜色，
                outlineColor: Cesium.Color.BLACK,
                // outlineColor:,
                outlineWidth: 9,
            }
        })
        // viewer.flyTo(point) // 飞行到指定点位的位置
        viewer.zoomTo(point) // 切换到指定点位的位置
    }
    CesiumEcharts(option) {

        /*
            1. 创建一个div用来渲染我们的echarts
            2. 对div进行设置里面的样式
            3. * 设置当前div无法选中 pointerEvents : none
        */

        // 这里是固定写法
           (function (e) {
               const t = {};
               function n(r) {
                   if (t[r]) return t[r].exports;
                   const i = t[r] = {
                       i: r,
                       l: !1,
                       exports: {}
                   };
                   return e[r].call(i.exports, i, i.exports, n),
                       i.l = !0,
                       i.exports
               }
               n.m = e,
                   n.c = t,
                   n.d = function (e, t, r) {
                       n.o(e, t) || Object.defineProperty(e, t, {
                           enumerable: !0,
                           get: r
                       })
                   },
                   n.r = function (e) {
                       "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
                           value: "Module"
                       }),
                           Object.defineProperty(e, "__esModule", {
                               value: !0
                           })
                   },
                   n.t = function (e, t) {
                       if (1 & t && (e = n(e)), 8 & t) return e;
                       if (4 & t && "object" == typeof e && e && e.__esModule) return e;
                       const r = Object.create(null);
                       if (n.r(r), Object.defineProperty(r, "default", {
                           enumerable: !0,
                           value: e
                       }), 2 & t && "string" != typeof e) for (let i in e) n.d(r, i,
                           function (t) {
                               return e[t]
                           }.bind(null, i));
                       return r
                   },
                   n.n = function (e) {
                       let t = e && e.__esModule ?
                           function () {
                               return e.
                                   default
                           } :
                           function () {
                               return e
                           };
                       return n.d(t, "a", t),
                           t
                   },
                   n.o = function (e, t) {
                       return Object.prototype.hasOwnProperty.call(e, t)
                   },
                   n.p = "",
                   n(n.s = 0)
           })([function (e, t, n) { e.exports = n(1) }, function (e, t, n) {
               echarts ? n(2).load() : console.error("没有找到echarts插件！！！")
           }, function (e, t, n) {
               // "use strict";
               function r(e, t) {
                   for (let n = 0; n < t.length; n++) {
                       let r = t[n];
                       r.enumerable = r.enumerable || !1,
                           r.configurable = !0,
                           "value" in r && (r.writable = !0),
                           Object.defineProperty(e, r.key, r)
                   }
               }
               n.r(t);
               echarts.cesiumViewer = this.viewer
               let i = function () {
                   function e(t, n) {
                       !
                           function (e, t) {
                               if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
                           }(this, e),
                           this._viewer = t,
                           this.dimensions = ["lng", "lat"],
                           this._mapOffset = [0, 0],
                           this._api = n
                   }
                   let t, n, i;
                   return t = e,
                       i = [{
                           key: "create",
                           value: function (t, n) {
                               let r;
                               t.eachComponent("GLMap",
                                   function (t) {
                                       (r = new e(echarts.cesiumViewer, n)).setMapOffset(t.__mapOffset || [0, 0]),
                                           t.coordinateSystem = r
                                   }),
                                   t.eachSeries(function (e) {
                                       "GLMap" === e.get("coordinateSystem") && (e.coordinateSystem = r)
                                   })
                           }
                       },
                       {
                           key: "dimensions",
                           get: function () {
                               return ["lng", "lat"]
                           }
                       }],
                       (n = [{
                           key: "setMapOffset",
                           value: function (e) {
                               return this._mapOffset = e,
                                   this
                           }
                       },
                       {
                           key: "getViewer",
                           value: function () {
                               return this._viewer
                           }
                       },
                       {
                           key: "dataToPoint",
                           value: function (e) {
                               let t = this._viewer.scene,
                                   n = [0, 0],
                                   r = Cesium.Cartesian3.fromDegrees(e[0], e[1]);
                               if (!r) return n;
                               if (t.mode === Cesium.SceneMode.SCENE3D && Cesium.Cartesian3.angleBetween(t.camera.position, r) > Cesium.Math.toRadians(80)) return !1;
                               let i = t.cartesianToCanvasCoordinates(r);
                               return i ? [i.x - this._mapOffset[0], i.y - this._mapOffset[1]] : n
                           }
                       },
                       {
                           key: "pointToData",
                           value: function (e) {
                               let t = this._mapOffset,
                                   n = viewer.scene.globe.ellipsoid,
                                   r = new Cesium.cartesian3(e[1] + t, e[2] + t[2], 0),
                                   i = n.cartesianToCartographic(r);
                               return [i.lng, i.lat]
                           }
                       },
                       {
                           key: "getViewRect",
                           value: function () {
                               let e = this._api;
                               return new echarts.graphic.BoundingRect(0, 0, e.getWidth(), e.getHeight())
                           }
                       },
                       {
                           key: "getRoamTransform",
                           value: function () {
                               return echarts.matrix.create()
                           }
                       }]) && r(t.prototype, n),
                       i && r(t, i),
                       e
               }();
               echarts.extendComponentModel({
                   type: "GLMap",
                   getViewer: function () {
                       return echarts.cesiumViewer
                   },
                   defaultOption: {
                       roam: !1
                   }
               }),
                   echarts.extendComponentView({
                       type: "GLMap",
                       init: function (e, t) {
                           this.api = t,
                               echarts.cesiumViewer.scene.postRender.addEventListener(this.moveHandler, this)
                       },
                       moveHandler: function (e, t) {
                           this.api.dispatchAction({
                               type: "GLMapRoam"
                           })
                       },
                       render: function (e, t, n) { },
                       dispose: function (e) {
                           echarts.cesiumViewer.scene.postRender.removeEventListener(this.moveHandler, this)
                       }
                   });
               function a() {
                   echarts.registerCoordinateSystem("GLMap", i),
                       echarts.registerAction({
                           type: "GLMapRoam",
                           event: "GLMapRoam",
                           update: "updateLayout"
                       },
                           function (e, t) { })
               }
               n.d(t, "load",
                   function () {
                       return a
                   })
           }])
           echarts.cesiumViewer = viewer;
        // 创建空div
        let div = document.createElement('div')
        div.style.position = 'absolute';
        div.style.left = '0px';
        div.style.top = '0px';
        div.style.width = '100%';
        div.style.height = "100%";
        div.style.pointerEvents = 'none';
        div.setAttribute('class', 'cesium_echarts')
        //  设置id
        let divLength = document.getElementsByClassName('cesium_echarts').length;
        let divID = `cesium_echarts_${mathRandom(0, 100000)}-${divLength}`
        div.setAttribute('id', divID);
        //    添加节点
        viewer.container.appendChild(div);
        echarts.init(document.getElementById(divID)).setOption(option)
        //  生成一个随机数n到m的随机数
        function mathRandom(n, m) {
            return parseInt(Math.random() * (n - m + 1) + m);
        }
        // createDiv(option)
    }
}

export default new Entity()