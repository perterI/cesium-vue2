import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    alias: '/home',
    name: 'home',
    component: HomeView,
    children: [{
      path: '/camera',
      name: 'CesiumCamera',
      component: () => import(/* webpackChunkName: "cesiumCamera" */ '../views/cesiumCamera.vue')
    },{
      path: '/model',
      name: 'CesiumModel',
      component: () => import(/* webpackChunkName: "cesiumAddEntity" */ '../views/addEntity.vue')
    },]
  },

]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
